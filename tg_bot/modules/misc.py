import html
import json
import random
from datetime import datetime
from typing import Optional, List
import time
import requests
from telegram import Message, Chat, Update, Bot, MessageEntity
from telegram import ParseMode
from telegram.ext import CommandHandler, run_async, Filters
from telegram.utils.helpers import escape_markdown, mention_html

from tg_bot import dispatcher, OWNER_ID, SUDO_USERS, SUPPORT_USERS, WHITELIST_USERS, BAN_STICKER
from tg_bot.__main__ import STATS, USER_INFO
from tg_bot.modules.disable import DisableAbleCommandHandler
from tg_bot.modules.helper_funcs.extraction import extract_user
from tg_bot.modules.helper_funcs.filters import CustomFilters
import tg_bot.modules.sql.main_sql as sqll

RUN_STRINGS = (
    "Where do you think you're going?",
    "Huh? what? did they get away?",
    "ZZzzZZzz... Huh? what? oh, just them again, nevermind.",
    "Get back here!",
    "Not so fast...",
    "Look out for the wall!",
    "Don't leave me alone with them!!",
    "You run, you die.",
    "Jokes on you, I'm everywhere",
    "You're gonna regret that...",
    "You could also try /kickme, I hear that's fun.",
    "Go bother someone else, no-one here cares.",
    "You can run, but you can't hide.",
    "Is that all you've got?",
    "I'm behind you...",
    "You've got company!",
    "We can do this the easy way, or the hard way.",
    "You just don't get it, do you?",
    "Yeah, you better run!",
    "Please, remind me how much I care?",
    "I'd run faster if I were you.",
    "That's definitely the droid we're looking for.",
    "May the odds be ever in your favour.",
    "Famous last words.",
    "And they disappeared forever, never to be seen again.",
    "\"Oh, look at me! I'm so cool, I can run from a bot!\" - this person",
    "Yeah yeah, just tap /kickme already.",
    "Here, take this ring and head to Mordor while you're at it.",
    "Legend has it, they're still running...",
    "Unlike Harry Potter, your parents can't protect you from me.",
    "Fear leads to anger. Anger leads to hate. Hate leads to suffering. If you keep running in fear, you might "
    "be the next Vader.",
    "Multiple calculations later, I have decided my interest in your shenanigans is exactly 0.",
    "Legend has it, they're still running.",
    "Keep it up, not sure we want you here anyway.",
    "You're a wiza- Oh. Wait. You're not Harry, keep moving.",
    "NO RUNNING IN THE HALLWAYS!",
    "Hasta la vista, baby.",
    "Who let the dogs out?",
    "It's funny, because no one cares.",
    "Ah, what a waste. I liked that one.",
    "Frankly, my dear, I don't give a damn.",
    "My milkshake brings all the boys to yard... So run faster!",
    "You can't HANDLE the truth!",
    "A long time ago, in a galaxy far far away... Someone would've cared about that. Not anymore though.",
    "Hey, look at them! They're running from the inevitable banhammer... Cute.",
    "Han shot first. So will I.",
    "What are you running after, a white rabbit?",
    "As The Doctor would say... RUN!",
)

RUN_STRINGS_RU = (
    "Куда ты собрался?",
    "А? что? они ушли?",
    "ZZzzZZzz... А? что? о, опять только они, ничего страшного.",
    "Вернись сюда!",
    "Не так быстро...",
    "Посмотри на стену!",
    "Не оставляй меня наедине с ними!!",
    "Ты бежишь, ты умираешь.",
    "Ты пожалеешь об этом...",
    "Вы также можете попробовать / kickme, я слышал, что это весело.",
    "Иди, побеспокой кого-нибудь другого, здесь всем плевать.",
    "Ты можешь бежать, но ты не можешь спрятаться.",
    "Это всё, что у тебя есть?",
    "Я стою за тобой...",
    "У вас есть компания!",
    "Мы можем сделать это лёгким или трудным путем.",
    "Ты просто не понимаешь этого, не так ли?",
    "Да, тебе лучше бежать!",
    "Пожалуйста, напомните мне, как меня это волнует?",
    "На твоем месте я бы бежал быстрее.",
    "Это определённо тот дроид, которого мы ищем.",
    "Пусть шансы всегда будут в вашу пользу.",
    "Знаменитые последние слова.",
    "И они исчезли навсегда, чтобы их больше не видели.",
    "\"О, посмотри на меня! Я такой крутой, что могу убежать от бота! \"- это человек"
    "Да, да, просто нажмите /kickme уже.",
    "Вот, возьмите это кольцо и отправляйтесь в Мордор, раз уж вам по пути.",
    "Легенда гласит, что они всё ещё бегут...",
    "В отличие от Гарри Поттера, твои родители не могут защитить тебя от меня.",
    "Страх приводит к гневу. Гнев приводит к ненависти. Ненависть приводит к страданиям. Если вы продолжите бежать в страхе, вы можете "
    "стать следующим Вейдером.",
    "Несколько вычислений спустя, я решил, что мой интерес к вашим махинациям ровно 0.",
    "Легенда гласит, что они всё ещё бегут.",
    "Продолжайте, не уверен, что мы хотим, чтобы вы здесь оставались.",
    "Ты волше-Ой. Погоди. Ты не Гарри, продолжай двигаться.",
    "НЕ БЕГАТЬ ПО КОРИДОРАМ!",
    "Hasta la vista, детка.",
    "Кто выпустил собак?",
    "Это смешно, потому что никому нет дела.",
    "Ах, какая потеря. Он мне нрав/runsился.",
    "Честно говоря, моя дорогая, мне наплевать.",
    "Мой молочный коктейль приводит всех мальчиков во двор... Так беги быстрее!",
    "Вы не можете справиться с правдой!",
    "Давным-давно, в далёкой-далёкой галактике... Кому-то было бы до этого дело. Хотя уже нет.",
    "Эй, посмотри на них! Они убегают от неизбежного банхаммера... Мило.",
    "Хан выстрелил первым. Я поступлю так же, как он.",
    "За чем ты бежишь, за белым кроликом?",
    "Как сказал бы Доктор... Беги!",
)

SLAP_TEMPLATES = (
    "{user1} {hits} {user2} with a {item}.",
    "{user1} {hits} {user2} in the face with a {item}.",
    "{user1} {hits} {user2} around a bit with a {item}.",
    "{user1} {throws} a {item} at {user2}.",
    "{user1} grabs a {item} and {throws} it at {user2}'s face.",
    "{user1} launches a {item} in {user2}'s general direction.",
    "{user1} starts slapping {user2} silly with a {item}.",
    "{user1} pins {user2} down and repeatedly {hits} them with a {item}.",
    "{user1} grabs up a {item} and {hits} {user2} with it.",
    "{user1} ties {user2} to a chair and {throws} a {item} at them.",
    "{user1} gave a friendly push to help {user2} learn to swim in lava."
)

ITEMS = (
    "cast iron skillet",
    "large trout",
    "baseball bat",
    "cricket bat",
    "wooden cane",
    "nail",
    "printer",
    "shovel",
    "CRT monitor",
    "physics textbook",
    "toaster",
    "portrait of Richard Stallman",
    "television",
    "five ton truck",
    "roll of duct tape",
    "book",
    "laptop",
    "old television",
    "sack of rocks",
    "rainbow trout",
    "rubber chicken",
    "spiked bat",
    "fire extinguisher",
    "heavy rock",
    "chunk of dirt",
    "beehive",
    "piece of rotten meat",
    "bear",
    "ton of bricks",
)

THROW = (
    "throws",
    "flings",
    "chucks",
    "hurls",
)

HIT = (
    "hits",
    "whacks",
    "slaps",
    "smacks",
    "bashes",
)

SLAP_TEMPLATES_RU = (
    "{user1} {hits} {user2} {itemp}.",
    "{user1} {hits} в лицо {user2} {itemp}.",
    "{user1} {throws} {item} в {user2}.",
    "{user1} берёт {item} и {throws} им {user2} в лицо.",
    "{user1} запускает {itemr} в направлении {user2}.",
    "{user1} начинает безобидно хлопать {user2} {itemp}.",
    "{user1} придавливает {user2} и несколько раз {hits} его {itemp}.",
    "{user1} хвататет {item} и {hits} {user2}",
    "{user1} привязывает {user2} к стулу и {throws} {itemr} в него.",
    "{user1} дружески подтолкнул {user2}, чтобы тот научился плавать в лаве."
)

ITEMSP_RU =  (
    "чугунной сковородой",
    "большой форелью",
    "бейсбольной битой",
    "крикетной битой",
    "деревянной тростью",
    "ногтем",
    "принтером",
    "лопатой",
    "ЭЛТ-монитором",
    "учебником физики ",
    "тостером",
    "портретом Ричарда Столмана",
    "телевизором",
    "пятитонным грузовиком",
    "рулоном скотча",
    "книгой",
    "ноутбуком",
    "старым телевизором",
    "мешком камней",
    "радужной форелью",
    "резиновым цыпленоком",
    "шипастой летучей мышью",
    "огнетушителем",
    "тяжёлым камнем",
    "кусоком грязи",
    "улеем",
    "кусоком гнилого мяса",
    "медведем",
    "тонной кирпичей",
    "огромной булкой",
)

ITEMS_RU =  (
    "чугунная сковорода",
    "большая форель",
    "бейсбольная бита",
    "крикетная бита",
    "деревянная трость",
    "ноготь",
    "принтер",
    "лопата",
    "ЭЛТ-монитор",
    "учебник физики ",
    "тостер",
    "портрет Ричарда Столмана",
    "телевизор",
    "пятитонный грузовик",
    "рулон клейкой ленты",
    "книга",
    "ноутбук",
    "старый телевизор",
    "мешок камней",
    "радужная форель",
    "резиновый цыпленок",
    "шипастая летучая мышь",
    "огнетушитель",
    "тяжёлый камень",
    "кусок грязи",
    "улей",
    "кусок гнилого мяса",
    "медведь",
    "тонна кирпичей",
)

ITEMSR_RU =  (
    "чугунную сковородку",
    "большкую форель",
    "бейсбольную биту",
    "крикетную биту",
    "деревянную трость",
    "ноготь",
    "принтер",
    "лопата",
    "ЭЛТ-монитор",
    "учебник физики ",
    "тостер",
    "портрет Ричарда Столмана",
    "телевизор",
    "пятитонный грузовик",
    "рулон клейкой ленты",
    "книга",
    "ноутбук",
    "старый телевизор",
    "мешок камней",
    "радужная форель",
    "резиновый цыпленок",
    "шипастая летучая мышь",
    "огнетушитель",
    "тяжёлый камень",
    "кусок грязи",
    "улей",
    "кусок гнилого мяса",
    "медведь",
    "тонну кирпичей",
)

THROW_RU =  (
    "бросает",
    "швыряет",
   )

HIT_RU =  (
    "бьёт",
    "молотит",
    "шлёпает",
    "хлопает",
    "колотит",
    "царапает",
)

GMAPS_LOC = "https://maps.googleapis.com/maps/api/geocode/json"
GMAPS_TIME = "https://maps.googleapis.com/maps/api/timezone/json"


@run_async
def runs(bot: Bot, update: Update):
    chat = update.effective_chat  # type: Optional[Chat]
    if sqll.chat_lang(chat.id):
        update.effective_message.reply_text(random.choice(RUN_STRINGS_RU))
    else:
        update.effective_message.reply_text(random.choice(RUN_STRINGS))


@run_async
def stickerid(bot: Bot, update: Update):
    msg = update.effective_message
    if msg.reply_to_message and msg.reply_to_message.sticker:
        update.effective_message.reply_text("Hello " +
                                            "[{}](tg://user?id={})".format(msg.from_user.first_name, msg.from_user.id)
                                            + ", The sticker id you are replying is :\n```" + 
                                            escape_markdown(msg.reply_to_message.sticker.file_id) + "```",
                                            parse_mode=ParseMode.MARKDOWN)
    else:
        update.effective_message.reply_text("Hello " + "[{}](tg://user?id={})".format(msg.from_user.first_name,
                                            msg.from_user.id) + ", Please reply to sticker message to get id sticker",
                                            parse_mode=ParseMode.MARKDOWN)
@run_async
def getsticker(bot: Bot, update: Update):
    msg = update.effective_message
    chat_id = update.effective_chat.id
    if msg.reply_to_message and msg.reply_to_message.sticker:
        bot.sendChatAction(chat_id, "typing")
        update.effective_message.reply_text("Hello " + "[{}](tg://user?id={})".format(msg.from_user.first_name,
                                            msg.from_user.id) + ", Please check the file you requested below."
                                            "\nPlease use this feature wisely!",
                                            parse_mode=ParseMode.MARKDOWN)
        bot.sendChatAction(chat_id, "upload_document")
        file_id = msg.reply_to_message.sticker.file_id
        newFile = bot.get_file(file_id)
        newFile.download('sticker.png')
        bot.sendDocument(chat_id, document=open('sticker.png', 'rb'))
        bot.sendChatAction(chat_id, "upload_photo")
        bot.send_photo(chat_id, photo=open('sticker.png', 'rb'))
        
    else:
        bot.sendChatAction(chat_id, "typing")
        update.effective_message.reply_text("Hello " + "[{}](tg://user?id={})".format(msg.from_user.first_name,
                                            msg.from_user.id) + ", Please reply to sticker message to get sticker image",
                                            parse_mode=ParseMode.MARKDOWN)
@run_async
def slap(bot: Bot, update: Update, args: List[str]):
    chat = update.effective_chat  # type: Optional[Chat]
    msg = update.effective_message  # type: Optional[Message]

    # reply to correct message
    reply_text = msg.reply_to_message.reply_text if msg.reply_to_message else msg.reply_text

    # get user who sent message
    if msg.from_user.username:
        curr_user = "@" + escape_markdown(msg.from_user.username)
    else:
        curr_user = "[{}](tg://user?id={})".format(msg.from_user.first_name, msg.from_user.id)

    user_id = extract_user(update.effective_message, args)
    if user_id:
        slapped_user = bot.get_chat(user_id)
        user1 = curr_user
        if slapped_user.username:
            user2 = "@" + escape_markdown(slapped_user.username)
        else:
            user2 = "[{}](tg://user?id={})".format(slapped_user.first_name,
                                                   slapped_user.id)

    # if no target found, bot targets the sender
    else:
        user1 = "[{}](tg://user?id={})".format(bot.first_name, bot.id)
        user2 = curr_user

    temp = random.choice(SLAP_TEMPLATES)
    item = random.choice(ITEMS)
    hit = random.choice(HIT)
    throw = random.choice(THROW)

    temp_ru = random.choice(SLAP_TEMPLATES_RU)
    item_ru = random.choice(ITEMS_RU)
    itemp_ru = random.choice(ITEMSP_RU)
    itemr_ru = random.choice(ITEMSR_RU)
    hit_ru = random.choice(HIT_RU)
    throw_ru = random.choice(THROW_RU)

    repl = temp.format(user1=user1, user2=user2, item=item, hits=hit, throws=throw)
    repl_ru = temp_ru.format(user1=user1, user2=user2, item=item_ru, hits=hit_ru, throws=throw_ru, itemp=itemp_ru, itemr=itemr_ru)

    if sqll.chat_lang(chat.id):
        reply_text(repl_ru, parse_mode=ParseMode.MARKDOWN)
    else:
        reply_text(repl, parse_mode=ParseMode.MARKDOWN)
    


@run_async
def get_bot_ip(bot: Bot, update: Update):
    """ Sends the bot's IP address, so as to be able to ssh in if necessary.
        OWNER ONLY.
    """
    res = requests.get("http://ipinfo.io/ip")
    update.message.reply_text(res.text)


@run_async
def get_id(bot: Bot, update: Update, args: List[str]):
    user_id = extract_user(update.effective_message, args)
    if user_id:
        if update.effective_message.reply_to_message and update.effective_message.reply_to_message.forward_from:
            user1 = update.effective_message.reply_to_message.from_user
            user2 = update.effective_message.reply_to_message.forward_from
            update.effective_message.reply_text(
                "The original sender, {}, has an ID of `{}`.\nThe forwarder, {}, has an ID of `{}`.".format(
                    escape_markdown(user2.first_name),
                    user2.id,
                    escape_markdown(user1.first_name),
                    user1.id),
                parse_mode=ParseMode.MARKDOWN)
        else:
            user = bot.get_chat(user_id)
            update.effective_message.reply_text("{}'s id is `{}`.".format(escape_markdown(user.first_name), user.id),
                                                parse_mode=ParseMode.MARKDOWN)
    else:
        chat = update.effective_chat  # type: Optional[Chat]
        if chat.type == "private":
            update.effective_message.reply_text("Your id is `{}`.".format(chat.id),
                                                parse_mode=ParseMode.MARKDOWN)

        else:
            update.effective_message.reply_text("This group's id is `{}`.".format(chat.id),
                                                parse_mode=ParseMode.MARKDOWN)


@run_async
def info(bot: Bot, update: Update, args: List[str]):
    msg = update.effective_message  # type: Optional[Message]
    user_id = extract_user(update.effective_message, args)
    chat = update.effective_chat  # type: Optional[Chat]

    if user_id:
        user = bot.get_chat(user_id)

    elif not msg.reply_to_message and not args:
        user = msg.from_user

    elif not msg.reply_to_message and (not args or (
            len(args) >= 1 and not args[0].startswith("@") and not args[0].isdigit() and not msg.parse_entities(
        [MessageEntity.TEXT_MENTION]))):
        msg.reply_text("I can't extract a user from this.")
        return

    else:
        return
    if sqll.chat_lang(chat.id):
        text = "<b>Информация о пользователе</b>:" \
            "\nID: <code>{}</code>" \
            "\nИмя: {}".format(user.id, html.escape(user.first_name))

        if user.last_name:
            text += "\nФамилия: {}".format(html.escape(user.last_name))

        if user.username:
            text += "\nНик: @{}".format(html.escape(user.username))

        text += "\nПостоянная ссылка на пользователся: {}".format(mention_html(user.id, "ссылка"))

        if user.id == OWNER_ID:
            text += "\n\nЭтот человек мой создатель, я бы не стала делать ему ничего плохого"
        else:
            if user.id in SUDO_USERS:
                text += "\nЭтот человек в sudo базе! " \
                        "У него много власти, смотрите!."
            else:
                if user.id in SUPPORT_USERS:
                    text += "\nЭтот человек в списке поддерживаемых пользователей! " \
                            "Он может вас заГБанить."

                if user.id in WHITELIST_USERS:
                    text += "\nЭтот человек в белом списке! " \
                            "Это значить что я не буду его варнить/банить."
    else:
        text = "<b>User info</b>:" \
            "\nID: <code>{}</code>" \
            "\nFirst Name: {}".format(user.id, html.escape(user.first_name))

        if user.last_name:
            text += "\nLast Name: {}".format(html.escape(user.last_name))

        if user.username:
            text += "\nUsername: @{}".format(html.escape(user.username))

        text += "\nPermanent user link: {}".format(mention_html(user.id, "link"))

        if user.id == OWNER_ID:
            text += "\n\nThis person is my owner - I would never do anything against them!"
        else:
            if user.id in SUDO_USERS:
                text += "\nThis person is one of my sudo users! " \
                        "Nearly as powerful as my owner - so watch it."
            else:
                if user.id in SUPPORT_USERS:
                    text += "\nThis person is one of my support users! " \
                            "Not quite a sudo user, but can still gban you off the map."

                if user.id in WHITELIST_USERS:
                    text += "\nThis person has been whitelisted! " \
                            "That means I'm not allowed to ban/kick them."

    for mod in USER_INFO:
        mod_info = mod.__user_info__(user.id).strip()
        if mod_info:
            text += "\n\n" + mod_info

    update.effective_message.reply_text(text, parse_mode=ParseMode.HTML)


@run_async
def get_time(bot: Bot, update: Update, args: List[str]):
    location = " ".join(args)
    chat = update.effective_chat  # type: Optional[Chat]
    if location.lower() == bot.first_name.lower():
        if sqll.chat_lang(chat.id):
            update.effective_message.reply_text("Время для банхаммера есть всегда!")
        else:
            update.effective_message.reply_text("Its always banhammer time for me!")
        bot.send_sticker(update.effective_chat.id, BAN_STICKER)
        return

    res = requests.get(GMAPS_LOC, params=dict(address=location))

    if res.status_code == 200:
        loc = json.loads(res.text)
        if loc.get('status') == 'OK':
            lat = loc['results'][0]['geometry']['location']['lat']
            long = loc['results'][0]['geometry']['location']['lng']

            country = None
            city = None

            address_parts = loc['results'][0]['address_components']
            for part in address_parts:
                if 'country' in part['types']:
                    country = part.get('long_name')
                if 'administrative_area_level_1' in part['types'] and not city:
                    city = part.get('long_name')
                if 'locality' in part['types']:
                    city = part.get('long_name')

            if city and country:
                location = "{}, {}".format(city, country)
            elif country:
                location = country

            timenow = int(datetime.utcnow().strftime("%s"))
            res = requests.get(GMAPS_TIME, params=dict(location="{},{}".format(lat, long), timestamp=timenow))
            if res.status_code == 200:
                offset = json.loads(res.text)['dstOffset']
                timestamp = json.loads(res.text)['rawOffset']
                time_there = datetime.fromtimestamp(timenow + timestamp + offset).strftime("%H:%M:%S on %A %d %B")
                update.message.reply_text("It's {} in {}".format(time_there, location))


@run_async
def echo(bot: Bot, update: Update):
    args = update.effective_message.text.split(None, 1)
    message = update.effective_message
    message.delete()
    if message.reply_to_message:
        message.reply_to_message.reply_text(args[1])
    else:
        message.reply_text(args[1], quote=False)

def ping(bot: Bot, update: Update):
    start_time = time.time()
    end_time = time.time()
    ping_time = float(end_time - start_time)*1000
    update.effective_message.reply_text("Pong!"
                                        "\nPing speed was : {}ms".format(ping_time))



MARKDOWN_HELP = """
Markdown is a very powerful formatting tool supported by telegram. {} has some enhancements, to make sure that \
saved messages are correctly parsed, and to allow you to create buttons.

- <code>_italic_</code>: wrapping text with '_' will produce italic text
- <code>*bold*</code>: wrapping text with '*' will produce bold text
- <code>`code`</code>: wrapping text with '`' will produce monospaced text, also known as 'code'
- <code>[sometext](someURL)</code>: this will create a link - the message will just show <code>sometext</code>, \
and tapping on it will open the page at <code>someURL</code>.
EG: <code>[test](example.com)</code>

- <code>[buttontext](buttonurl:someURL)</code>: this is a special enhancement to allow users to have telegram \
buttons in their markdown. <code>buttontext</code> will be what is displayed on the button, and <code>someurl</code> \
will be the url which is opened.
EG: <code>[This is a button](buttonurl:example.com)</code>

If you want multiple buttons on the same line, use :same, as such:
<code>[one](buttonurl://example.com)
[two](buttonurl://google.com:same)</code>
This will create two buttons on a single line, instead of one button per line.

Keep in mind that your message <b>MUST</b> contain some text other than just a button!
""".format(dispatcher.bot.first_name)

MARKDOWN_HELP_RU = """
Markdown - очень мощный инструмент для форматирования текста, который поддерживает Telegram. {} имеет некоторые улучшения, чтобы убедиться, что 
сохраненные сообщения правильно написаны, что позволяет создавать кнопки.

- <code>_italic _</code>: выделение с двух сторон текста с помощью «_» приведет к созданию курсивного текста.
- <code> * bold * </code>: выделение с двух сторон текста с помощью '*' приведет к получению жирного текста.
- <code> code </code>: выделение с двух сторон текста с помощью '`' приведет к получению моноширинного текста, также известного как «код»,
- <code>[ваш_текст] (ваша_ссылка) </code>:  это создаст ссылку - сообщение просто покажет <code> ваш_текст </code>, \
и нажатие на него откроет страницу в <code> ваша_ссылка </code>.
EG: <code> [test] (ваша_ссылка) </code>

- <code> [Текст кнопки] (buttonurl: ваша ссылка) </code>: это специальное расширение, позволяющее пользователям создавать кнопки-ссылки. <code> Текст Кнопки </code> будет отображаться на кнопке, а <code> ваша ссылка </code> 
будет открывать ваш URL-адрес.
EG: <code> [Это кнопка] (buttonurl: это_ссылка) </code>

Если вам нужно несколько кнопок в одной строке, используйте это:
<code> [one] (buttonurl: //ваша_ссылка)
[two] (buttonurl: //google.com:same)</code>
Это создаст две кнопки в одной строке, а не одну кнопку на строку.

Имейте в виду, что ваше сообщение <b>ДОЛЖНО</b> содержать текст, отличный от кнопки!
""".format(dispatcher.bot.first_name)

MATH_HELP = """
Solves complex math problems using https://newton.now.sh

 - /simplify: Simplify `/simplify 2^2+2(2)`
 - /factor: Factor `/factor x^2 + 2x`
 - /derive: Derive `/derive x^2+2x`
 - /integrate: Integrate `/integrate x^2+2x`
 - /zeroes: Find 0's `/zeroes x^2+2x`
 - /tangent: Find Tangent `/tangent 2lx^3`
 - /area: Area Under Curve `/area 2:4lx^3`
 - /cos: Cosine `/cos pi`
 - /sin: Sine `/sin 0`
 - /tan: Tangent `/tan 0`
 - /arccos: Inverse Cosine `/arccos 1`
 - /arcsin: Inverse Sine `/arcsin 0`
 - /arctan: Inverse Tangent `/arctan 0`
 - /abs: Absolute Value `/abs -1`
 - /log: Logarithm `/log 2l8`

__Keep in mind__: To find the tangent line of a function at a certain x value, send the request as c|f(x) where c is the given x value and f(x) is the function expression, the separator is a vertical bar '|'. See the table above for an example request.

To find the area under a function, send the request as c:d|f(x) where c is the starting x value, d is the ending x value, and f(x) is the function under which you want the curve between the two x values.

To compute fractions, enter expressions as numerator(over)denominator. For example, to process 2/4 you must send in your expression as 2(over)4. The result expression will be in standard math notation (1/2, 3/4).
"""

MATH_HELP_RU = """
Решает сложные математические задачи с помощью https://newton.now.sh

 - /simplify: Simplify /simplify 2^2+2(2)
 - /factor: Factor /factor x^2 + 2x
 - /derive: Derive /derive x^2+2x
 - /integrate: Integrate /integrate x^2+2x
 - /zeroes: Find 0's /zeroes x^2+2x
 - /tangent: Find Tangent /tangent 2lx^3
 - /area: Area Under Curve /area 2:4lx^3
 - /cos: Cosine /cos pi
 - /sin: Sine /sin 0
 - /tan: Tangent /tan 0
 - /arccos: Inverse Cosine /arccos 1
 - /arcsin: Inverse Sine /arcsin 0
 - /arctan: Inverse Tangent /arctan 0
 - /abs: Absolute Value /abs -1
 - /log: Logarithm /log 2l8

Имейте в виду: чтобы найти касательную линию функции при определенном значении x, отправьте запрос как c|f(x), где c-заданное значение x, а f(x) - выражение функции, разделитель-вертикальная полоса"|". Пример запроса см. в таблице выше.

Чтобы найти площадь под функцией, отправить запрос c:d/f(х) где C является исходным значением x, D-это конечные значения X и f(x) - функция, в которой будет Кривой между двумя значениями x.

Для вычисления дробей введите выражения в виде числитель(над)знаменатель. Например, для обработки 2/4 необходимо отправить выражение как 2(над)4. Выражение результата будет в стандартной математической записи (1/2, 3/4).
"""


@run_async
def markdown_help(bot: Bot, update: Update):
    chat = update.effective_chat  # type: Optional[Chat]
    if sqll.chat_lang(chat.id):
        update.effective_message.reply_text(MARKDOWN_HELP_RU, parse_mode=ParseMode.HTML)
        update.effective_message.reply_text("Попробуйте переслать сообщение ниже мне, и вы сами все увидите!")
        update.effective_message.reply_text("/save test Это тест разметки. _italics_, *bold*, `code`, "
                                            "[URL](example.com) [button](buttonurl:github.com) "
                                            "[button2](buttonurl://google.com:same)")   
    else:
        update.effective_message.reply_text(MARKDOWN_HELP, parse_mode=ParseMode.HTML)
        update.effective_message.reply_text("Try forwarding the following message to me, and you'll see!")
        update.effective_message.reply_text("/save test This is a markdown test. _italics_, *bold*, `code`, "
                                            "[URL](example.com) [button](buttonurl:github.com) "
                                            "[button2](buttonurl://google.com:same)")


@run_async
def math_help(bot: Bot, update: Update):
    chat = update.effective_chat  # type: Optional[Chat]
    if sqll.chat_lang(chat.id):
        update.effective_message.reply_text(MATH_HELP_RU, parse_mode=ParseMode.HTML)
    else:
        update.effective_message.reply_text(MATH_HELP, parse_mode=ParseMode.HTML)
@run_async
def trs(bot: Bot, update: Update):
    update.effective_message.reply_text("Я переведена с помощью этих замечательных парней:"
                                        "@MrYacha, @fishaffair, @Pipivi")


@run_async
def stats(bot: Bot, update: Update):
    update.effective_message.reply_text("Current stats:\n" + "\n".join([mod.__stats__() for mod in STATS]))


# /ip is for private use
__help__ = """
 - /id: get the current group id. If used by replying to a message, gets that user's id.
 - /runs: reply a random string from an array of replies.
 - /slap: slap a user, or get slapped if not a reply.
 - /time <place>: gives the local time at the given place.
 - /info: get information about a user.
 - /stickerid: reply sticker message to get id sticker.
 - /getsticker: reply sticker message to get sticker image.

 - /markdownhelp: quick summary of how markdown works in telegram - can only be called in private chats.
 - /mathhelp: Use math
 - /ping - wait time of a bot
"""

__help_ru__ = """
  - /id: получить текущий идентификатор группы. Если отвечать данной командой на сообщения, то выдаст идентификатор пользователя.
  - /runs: ответить на случайную строку из массива ответов.
  - /slap: ударить пользователя или получить ответ.
  - /time <место>: дает местное время.
  - /info: получить информацию о пользователе.
  - /stickerid: ответьте на стикер чтобы получить его id.
  - /getsticker: ответьте на сiooтикер чтобы получить изображение стикера.

  - /markdownhelp: краткое изложение того, как работает разметка в телеграмме, - может быть вызвано только в частных чатах.
  - /mathhelp: Использование математики
  - /ping: время ожидания бота
"""

__mod_name__ = "Misc"

ID_HANDLER = DisableAbleCommandHandler("id", get_id, pass_args=True)
IP_HANDLER = CommandHandler("ip", get_bot_ip, filters=Filters.chat(OWNER_ID))

TIME_HANDLER = CommandHandler("time", get_time, pass_args=True)

RUNS_HANDLER = DisableAbleCommandHandler("runs", runs)
SLAP_HANDLER = DisableAbleCommandHandler("slap", slap, pass_args=True)
INFO_HANDLER = DisableAbleCommandHandler("info", info, pass_args=True)

TRS_LIST = DisableAbleCommandHandler("trs", trs)

PING_HANDLER = DisableAbleCommandHandler("ping", ping)
STICKERID_HANDLER = DisableAbleCommandHandler("stickerid", stickerid)
GETSTICKER_HANDLER = DisableAbleCommandHandler("getsticker", getsticker)
ECHO_HANDLER = CommandHandler("echo", echo, filters=Filters.user(OWNER_ID))
MD_HELP_HANDLER = CommandHandler("markdownhelp", markdown_help, filters=Filters.private)
MATH_HELP_HANDLER = CommandHandler("mathhelp", math_help, filters=Filters.private)

STATS_HANDLER = CommandHandler("stats", stats, filters=CustomFilters.sudo_filter)

dispatcher.add_handler(ID_HANDLER)
dispatcher.add_handler(PING_HANDLER)
dispatcher.add_handler(IP_HANDLER)
dispatcher.add_handler(TIME_HANDLER)
dispatcher.add_handler(RUNS_HANDLER)
dispatcher.add_handler(SLAP_HANDLER)
dispatcher.add_handler(INFO_HANDLER)
dispatcher.add_handler(STICKERID_HANDLER)
dispatcher.add_handler(GETSTICKER_HANDLER)
dispatcher.add_handler(ECHO_HANDLER)
dispatcher.add_handler(MD_HELP_HANDLER)
dispatcher.add_handler(MATH_HELP_HANDLER)
dispatcher.add_handler(STATS_HANDLER)
dispatcher.add_handler(TRS_LIST)