from typing import Optional

import re

from telegram import Message, Update, Bot, User
from telegram import ParseMode, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.error import BadRequest
from telegram.ext import CommandHandler, run_async, Filters, CallbackQueryHandler
from telegram.utils.helpers import escape_markdown

import tg_bot.modules.sql.rules_sql as sql
from tg_bot import dispatcher
from tg_bot.modules.helper_funcs.chat_status import user_admin
from tg_bot.modules.helper_funcs.string_handling import markdown_parser
import tg_bot.modules.sql.main_sql as sqll


@run_async
def get_rules(bot: Bot, update: Update):
    chat_id = update.effective_chat.id
    send_rules(update, chat_id)


# Do not async - not from a handler
def send_rules(update, chat_id, from_pm=False):
    bot = dispatcher.bot
    user = update.effective_user  # type: Optional[User]
    chat = update.effective_chat  # type: Optional[Chat]
    try:
        chat = bot.get_chat(chat_id)
    except BadRequest as excp:
        if excp.message == "Chat not found" and from_pm:
            if sqll.chat_lang(chat.id):
                bot.send_message(user.id, "Ссылка на правила для этого чата не была настроена! Спросите админов чтобы исправить это")
            else:
                bot.send_message(user.id, "The rules shortcut for this chat hasn't been set properly! Ask admins to "
                                    "fix this.")
            return
        else:
            raise

    rules = sql.get_rules(chat_id)
    text = "The rules for *{}* are:\n\n{}".format(escape_markdown(chat.title), rules)

    if from_pm and rules:
        bot.send_message(user.id, text, parse_mode=ParseMode.MARKDOWN)
    elif from_pm:
        if sqll.chat_lang(chat.id):
            bot.send_message(user.id, "В этой группе не установлены правила. "
                                "Это, означает, что тут беззаконие, хотя...!")
        else:
            bot.send_message(user.id, "The group admins haven't set any rules for this chat yet. "
                                    "This probably doesn't mean it's lawless though...!")

    elif rules:
        if sqll.chat_lang(chat.id):
            update.effective_message.reply_text("Нажмине на кнопку ниже чтобы получить правила.",
                                                reply_markup=InlineKeyboardMarkup(
                                                    [[InlineKeyboardButton(text="Правила",
                                                                        callback_data="rules_txt({})".format(user.id))]]))
        else:
            update.effective_message.reply_text("Click on button below for receive rules.",
                                                reply_markup=InlineKeyboardMarkup(
                                                    [[InlineKeyboardButton(text="Rules",
                                                                        callback_data="rules_txt({})".format(user.id))]]))
    else:
        if sqll.chat_lang(chat.id):
            update.effective_message.reply_text("В этой группе не установлены правила. "
                                "Это, означает, что тут беззаконие, хотя...!")
        else:
            update.effective_message.reply_text("The group admins haven't set any rules for this chat yet. "
                                    "This probably doesn't mean it's lawless though...!")

@run_async
@user_admin
def set_rules(bot: Bot, update: Update):
    chat_id = update.effective_chat.id
    msg = update.effective_message  # type: Optional[Message]
    raw_text = msg.text
    args = raw_text.split(None, 1)  # use python's maxsplit to separate cmd and args
    chat = update.effective_chat  # type: Optional[Chat]
    if len(args) == 2:
        txt = args[1]
        offset = len(txt) - len(raw_text)  # set correct offset relative to command
        markdown_rules = markdown_parser(txt, entities=msg.parse_entities(), offset=offset)

        sql.set_rules(chat_id, markdown_rules)
        if sqll.chat_lang(chat.id):
            update.effective_message.reply_text("Успешно установлны правила для этой группы.")
        else:
            update.effective_message.reply_text("Successfully set rules for this group.")


@run_async
@user_admin
def clear_rules(bot: Bot, update: Update):
    chat_id = update.effective_chat.id
    sql.set_rules(chat_id, "")
    if sqll.chat_lang(chat.id):
        update.effective_message.reply_text("Успешно удалны правила!")
    else:
        update.effective_message.reply_text("Successfully cleared rules!")

@run_async
def button(bot: Bot, update: Update) -> str:
    query = update.callback_query  # type: Optional[CallbackQuery]
    user = update.effective_user  # type: Optional[User]
    chat_id = update.effective_chat.id
    chat = update.effective_chat  # type: Optional[Chat]
    match = re.match(r"rules_txt\((.+?)\)", query.data)

    rules = sql.get_rules(chat_id)
    text = "I sent to you rules in personal the message, check"
    text_ru = "Я отправила вам правила в личные сообщение, проверьте"
    if match:
        user_id = match.group(1)
        if sqll.chat_lang(chat.id):
            query.answer(text=text_ru, show_alert=True)
        else:
            query.answer(text=text, show_alert=True)
        bot.send_message(user.id, rules)



def __stats__():
    return "{} chats have rules set.".format(sql.num_chats())


def __import_data__(chat_id, data):
    # set chat rules
    rules = data.get('info', {}).get('rules', "")
    sql.set_rules(chat_id, rules)


def __migrate__(old_chat_id, new_chat_id):
    sql.migrate_chat(old_chat_id, new_chat_id)


def __chat_settings__(chat_id, user_id):
    return "This chat has had it's rules set: `{}`".format(bool(sql.get_rules(chat_id)))


__help__ = """
 - /rules: get the rules for this chat.

*Admin only:*
 - /setrules <your rules here>: set the rules for this chat.
 - /clearrules: clear the rules for this chat.
"""

__help_ru__ = """
- /rules: получить правила для этого чата.

*Только админы:*
 - /setrules <ваши правила здесь>: установить правила для этого чата.
 - /clearrules: очистить правила для этого чата.
"""

__mod_name__ = "Rules"

GET_RULES_HANDLER = CommandHandler("rules", get_rules, filters=Filters.group)
SET_RULES_HANDLER = CommandHandler("setrules", set_rules, filters=Filters.group)
RESET_RULES_HANDLER = CommandHandler("clearrules", clear_rules, filters=Filters.group)
CALLBACK_QUERY_HANDLER = CallbackQueryHandler(button, pattern=r"rules_txt")

dispatcher.add_handler(CALLBACK_QUERY_HANDLER)
dispatcher.add_handler(GET_RULES_HANDLER)
dispatcher.add_handler(SET_RULES_HANDLER)
dispatcher.add_handler(RESET_RULES_HANDLER)
